#include "gtest/gtest.h"
#include "test/SolverMock.h"
#include "XorDetector.h"

#include <iostream>

TEST(xordetection, twoXorOk) {
    std::vector<testImpl::Clause> clauses = {
        { 1, 2},
        {-1,-2}
    };

    XorDetector detector(2);


    for (Clause& clause: clauses) {
        detector.addClause(clause);
    }

    std::vector<Xor> xors = detector.findXors();
    EXPECT_EQ(xors.size(), 1);
    EXPECT_EQ(xors.back().rhs, true);
}

// TEST(xordetection, exactlyTwoOfFour) {
//     std::vector<Clause> clauses = {
//         {-1, -2, -3},
//         {-1, -2, -4},
//         {-1, -3, -4},
//         {-2, -3, -4},
//         { 1,  2,  3},
//         { 1,  2,  4},
//         { 1,  3,  4},
//         { 2,  3,  4}};

//     XorDetector detector(4);


//     for (Clause& clause: clauses) {
//         detector.addClause(clause);
//     }

//     std::vector<Xor> xors = detector.findXors();
//     EXPECT_EQ(xors.size(), 1);
//     if (xors.size() > 0) {
//         EXPECT_EQ(xors.back().rhs, false);
//     }
// }