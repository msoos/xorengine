#include "test/Solver.h"
#include "prooflogging/drat/drat_xor_prooflogging.h"


#include <sys/types.h>
#include <sys/stat.h>
#include <system_error>

#include <string>
#include <fstream>
#include <exception>


#include <functional>
#include <algorithm>

#include "gtest/gtest.h"
#include "misc.h"

using namespace proof::drat;
using namespace proof::drat::xr;

std::string createDirs(std::string name) {
    size_t pos = 0;
    while (pos < name.size()) {
        size_t end = name.find('/', pos);
        if (end == std::string::npos) {
            end = name.size();
        } else {
            std::string part = name.substr(0, end);
            int status = mkdir(part.c_str(), S_IRWXU | S_IRWXG);
            if (status != 0 && errno != EEXIST) {
                std::cout << "Failed to create directory for " << part << ":" << status << std::endl;
                exit(-1);
            }
        }
        pos = end + 1;
    }
    return name;
}

class CNF {
    std::ofstream file;
    size_t maxVar;
    size_t nClauses;

public:
    CNF(std::string fileName, size_t maxVar, size_t nClauses)
        : file(fileName)
        , maxVar(maxVar)
        , nClauses(nClauses)
    {
        if (!file)
            throw std::system_error(errno, std::system_category(), "failed to open " + fileName);


        file << "p cnf " << maxVar << " " << nClauses << std::endl;
    }

    ~CNF() {
        assert(nClauses == 0);
    }

    template<typename T>
    void add(T beginLits, T endLits) {
        assert(nClauses > 0);
        nClauses -= 1;
        T it = beginLits;
        while (it != endLits) {
            testImpl::Lit lit = *it;
            ++it;

            if (lit.isNegated()) {
                file << "-";
            }
            file << lit.var().value;
            assert(lit.var().value <= maxVar);
            file << " ";
        }
        file << "0" << std::endl;
    }

    void addAll(const std::vector<testImpl::Clause>& clauses) {
        for (const auto& cls : clauses) {
            add(cls.lits.begin(), cls.lits.end());
        }
    }
};



void addXor2cnf(const ShortXor& xr, CNF& cnf) {
    auto f = std::bind(&CNF::add<AddXorCallbackIt>, &cnf, std::placeholders::_1, std::placeholders::_2);
    addXor2X(xr, f);
}

void addXor2check(const ShortXor& xr, Proof& proof) {
    auto f = std::bind(&Proof::find<AddXorCallbackIt>, &proof, std::placeholders::_1, std::placeholders::_2);
    addXor2X(xr, f);
}



class TestCaseGen {
public:
    CNF cnf;
    Proof proof;

    TestCaseGen(std::string testName, size_t maxVar, size_t nClauses)
        : cnf(createDirs(std::string("generated_tests/drat/") + testName + std::string(".cnf")), maxVar, nClauses)
        , proof(std::string("generated_tests/drat/") + testName + std::string(".drat"), maxVar)
    {
    }

};

class CountHelper {
public:
    size_t nClauses = 0;
    size_t maxVar = 0;

    template<typename T>
    void addClauses(T begin, T end) {
        assert(begin != end);
        T it = begin;
        while (it != end) {
            const testImpl::Clause& cls = *it;
            ++it;
            nClauses += 1;
            for (testImpl::Lit lit: cls.lits) {
                maxVar = std::max(maxVar, static_cast<size_t>(lit.var()));
            }
        }
    }

    void addXor(const ShortXor& xr) {
        nClauses += 1 << (xr.vars.size() - 1);
        for (testImpl::Var var: xr.vars) {
            maxVar = std::max(maxVar, static_cast<size_t>(var));
        }
    }

    template<typename T>
    void addXors(T begin, T end) {
        assert(begin != end);
        T it = begin;
        while (it != end) {
            addXor(*it);
            ++it;
        }
    }

    void addLongXor(const LongXor& xr) {
        addXors(xr.xrs.begin(), xr.xrs.end());
    }

    template<typename T>
    void addLongXors(T begin, T end) {
        assert(begin != end);
        T it = begin;
        while (it != end) {
            addLongXor(*it);
            ++it;
        }
    }
};

TEST(drat_xor_prooflogging, addShort_1) {
    std::vector<ShortXor> xors;
    xors.push_back(ShortXor(true, {1,2,3,4}));
    xors.push_back(ShortXor(true, {1,2}));

    ShortXor expected(false, {3,4});

    CountHelper count;
    count.addXors(xors.begin(), xors.end());
    TestCaseGen test("addShort_1", count.maxVar, count.nClauses);
    for (const ShortXor& xr: xors) {addXor2cnf(xr, test.cnf);}


    ShortXor result = addShort(test.proof, xors[0], xors[1]);

    addXor2check(expected, test.proof);

    LOG(debug) << result << EOM;
    EXPECT_EQ(expected, result);
}

TEST(drat_xor_prooflogging, addShort_2) {
    std::vector<ShortXor> xors;
    xors.push_back(ShortXor(true, {1,2,3,4,5}));
    xors.push_back(ShortXor(true, {1,4,5}));

    ShortXor expected(false, {2,3});

    CountHelper count;
    count.addXors(xors.begin(), xors.end());
    TestCaseGen test("addShort_2", count.maxVar, count.nClauses);
    for (const ShortXor& xr: xors) {addXor2cnf(xr, test.cnf);}


    ShortXor result = addShort(test.proof, xors[0], xors[1]);

    addXor2check(expected, test.proof);

    LOG(debug) << result << EOM;
    EXPECT_EQ(expected, result);
}

TEST(drat_xor_prooflogging, addShort_3) {
    std::vector<ShortXor> xors;
    xors.push_back(ShortXor(true, {1,2,3,4,5}));
    xors.push_back(ShortXor(false, {1,2,3,4,5}));

    ShortXor expected(true, {});

    CountHelper count;
    count.addXors(xors.begin(), xors.end());
    TestCaseGen test("addShort_3", count.maxVar, count.nClauses);
    for (const ShortXor& xr: xors) {addXor2cnf(xr, test.cnf);}


    ShortXor result = addShort(test.proof, xors[0], xors[1]);

    addXor2check(expected, test.proof);

    LOG(debug) << result << EOM;
    EXPECT_EQ(expected, result);
}

TEST(drat_xor_prooflogging, addShort_4) {
    std::vector<ShortXor> xors;
    xors.push_back(ShortXor(true, {1}));
    xors.push_back(ShortXor(false, {1}));

    ShortXor expected(true, {});

    CountHelper count;
    count.addXors(xors.begin(), xors.end());
    TestCaseGen test("addShort_4", count.maxVar, count.nClauses);
    for (const ShortXor& xr: xors) {addXor2cnf(xr, test.cnf);}


    ShortXor result = addShort(test.proof, xors[0], xors[1]);

    addXor2check(expected, test.proof);

    EXPECT_EQ(expected, result);
}

TEST(drat_xor_prooflogging, addShort_5) {
    std::vector<ShortXor> xors;
    xors.push_back(ShortXor(false, {4,8,9}));
    xors.push_back(ShortXor(true, {2,3,6,8}));

    ShortXor expected(true, {2,3,4,6,9});

    CountHelper count;
    count.addXors(xors.begin(), xors.end());
    TestCaseGen test("addShort_5", count.maxVar, count.nClauses);
    for (const ShortXor& xr: xors) {addXor2cnf(xr, test.cnf);}


    ShortXor result = addShort(test.proof, xors[0], xors[1]);

    addXor2check(expected, test.proof);

    LOG(debug) << result << EOM;
    EXPECT_EQ(expected, result);
}

TEST(drat_xor_prooflogging, addShort_6) {
    std::vector<ShortXor> xors;
    xors.push_back(ShortXor(false, {1,2}));
    xors.push_back(ShortXor(true, {2,3}));

    ShortXor expected(true, {1,3});

    CountHelper count;
    count.addXors(xors.begin(), xors.end());
    TestCaseGen test("addShort_6", count.maxVar, count.nClauses);
    for (const ShortXor& xr: xors) {addXor2cnf(xr, test.cnf);}


    ShortXor result = addShort(test.proof, xors[0], xors[1]);

    addXor2check(expected, test.proof);

    LOG(debug) << result << EOM;
    EXPECT_EQ(expected, result);
}

TEST(drat_xor_prooflogging, longXorIt_1) {
    std::vector<ShortXor> xorsA;
    xorsA.push_back(ShortXor(true, {1,2,3}));
    LongXor a(std::move(xorsA));

    auto it = a.begin();
    EXPECT_EQ(*(it++), 1);
    ASSERT_NE(it, a.end());
    EXPECT_EQ(*(it++), 2);
    ASSERT_NE(it, a.end());
    EXPECT_EQ(*(it++), 3);
    EXPECT_EQ(it, a.end());
}

TEST(drat_xor_prooflogging, longXorIt_2) {
    std::vector<ShortXor> xorsA;
    xorsA.push_back(ShortXor(true, {1,2,5}));
    xorsA.push_back(ShortXor(true, {5,3,4}));
    LongXor a(std::move(xorsA));

    EXPECT_EQ(a.rhs(), false);

    auto it = a.begin();
    EXPECT_EQ(*(it++), 1);
    ASSERT_NE(it, a.end());
    EXPECT_EQ(*(it++), 2);
    ASSERT_NE(it, a.end());
    EXPECT_EQ(*(it++), 3);
    ASSERT_NE(it, a.end());
    EXPECT_EQ(*(it++), 4);
    EXPECT_EQ(it, a.end());
}

TEST(drat_xor_prooflogging, longXorIt_3) {
    std::vector<ShortXor> xorsA;
    xorsA.push_back(ShortXor(true, {}));
    LongXor a(std::move(xorsA));
    EXPECT_EQ(a.rhs(), true);

    auto it = a.begin();
    EXPECT_EQ(it, a.end());
}

TEST(drat_xor_prooflogging, addLong_1) {
    std::vector<ShortXor> xorsA;
    xorsA.push_back(ShortXor(true, {1,3,8}));
    xorsA.push_back(ShortXor(false, {4,8,9}));
    xorsA.push_back(ShortXor(true, {7,9}));
    LongXor a(std::move(xorsA));
    a.connectionVars = {8,8,9,9};

    std::vector<ShortXor> xorsB;
    xorsB.push_back(ShortXor(false, {1,2,6}));
    xorsB.push_back(ShortXor(true, {4,5,6}));
    xorsB.push_back(ShortXor(false, {5,7}));
    LongXor b(std::move(xorsB));
    b.connectionVars = {6,5,6,5};

    std::vector<ShortXor> xorsExpected = {
        ShortXor(true, {2,3})
    };

    LongXor expected(std::move(xorsExpected));


    CountHelper count;
    count.addLongXor(a);
    count.addLongXor(b);

    TestCaseGen test("addLong_1", count.maxVar, count.nClauses);

    for (const ShortXor& xr: a.xrs) {addXor2cnf(xr, test.cnf);}
    for (const ShortXor& xr: b.xrs) {addXor2cnf(xr, test.cnf);}

    LongXor result = addLong(test.proof, a, b);

    EXPECT_EQ(result, expected);
}

TEST(drat_xor_prooflogging, addLong_2) {
    std::vector<ShortXor> xorsA;
    xorsA.push_back(ShortXor(true, {1,3,8}));
    xorsA.push_back(ShortXor(false, {4,8,9}));
    xorsA.push_back(ShortXor(true, {7,9,10}));
    LongXor a(std::move(xorsA));
    a.connectionVars = {8,8,9,9};

    std::vector<ShortXor> xorsB;
    xorsB.push_back(ShortXor(false, {1,2,6}));
    xorsB.push_back(ShortXor(true, {4,5,6}));
    xorsB.push_back(ShortXor(false, {5,7,11,12}));
    LongXor b(std::move(xorsB));
    b.connectionVars = {6,5,6,5};

    std::vector<ShortXor> xorsExpected = {
        ShortXor(true, {2,3,10,11,12})
    };

    LongXor expected(std::move(xorsExpected));


    CountHelper count;
    count.addLongXor(a);
    count.addLongXor(b);

    TestCaseGen test("addLong_2", count.maxVar, count.nClauses);

    for (const ShortXor& xr: a.xrs) {addXor2cnf(xr, test.cnf);}
    for (const ShortXor& xr: b.xrs) {addXor2cnf(xr, test.cnf);}

    LongXor result = addLong(test.proof, a, b);

    EXPECT_EQ(result, expected);
}