#include "gtest/gtest.h"
#include "test/SolverMock.h"
#include "myXor.h"

#include <vector>

using ::testing::_;
using ::testing::AnyNumber;
using ::testing::Return;

using namespace xorp;

typedef std::vector<Lit> lv;

TEST(XorPropagator, unitXor_1) {
    std::vector<Xor> xors;
    xors.emplace_back(true, lv{Lit(0)});

    SolverMock solver;
    EXPECT_CALL(solver, conflict)
        .Times(0);
    EXPECT_CALL(solver, enqueue(Lit(0), PropEq(lv{Lit(0)})));

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
}

TEST(XorPropagator, unitXor_2) {
    std::vector<Xor> xors;
    xors.emplace_back(false, lv{Lit(1)});

    SolverMock solver;
    EXPECT_CALL(solver, conflict)
        .Times(0);
    EXPECT_CALL(solver, enqueue(~Lit(1), PropEq(lv{~Lit(1)})));
    EXPECT_CALL(solver, isSet(_))
        .Times(AnyNumber());
    EXPECT_CALL(solver, isSet(Var(0)))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
}

TEST(XorPropagator, empty_sat) {
    std::vector<Xor> xors;
    xors.emplace_back(false, lv{});

    SolverMock solver;
    EXPECT_CALL(solver, conflict)
        .Times(0);
    EXPECT_CALL(solver, isSet(Var(0)))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
}

TEST(XorPropagator, empty_unsat) {
    std::vector<Xor> xors;
    xors.emplace_back(true, lv{});

    SolverMock solver;
    EXPECT_CALL(solver, conflict(ConflictEq(lv{})))
        .Times(1);
    EXPECT_CALL(solver, isSet(Var(0)))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
}

TEST(XorPropagator, init_unsat) {
    std::vector<Xor> xors;
    xors.emplace_back(true, lv{Lit(0), Lit(1)});
    xors.emplace_back(false, lv{Lit(0), Lit(1)});

    SolverMock solver;
    EXPECT_CALL(solver, conflict(ConflictEq(lv{})))
        .Times(1);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
}

TEST(XorPropagator, init_propagate) {
    std::vector<Xor> xors;
    xors.emplace_back(true, lv{Lit(0), ~Lit(1)});
    xors.emplace_back(false, lv{Lit(0), ~Lit(1), Lit(2)});

    SolverMock solver;
    EXPECT_CALL(solver, enqueue(Lit(2), PropEq(lv{Lit(2)})));
    EXPECT_CALL(solver, conflict(_))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
}

TEST(XorPropagator, init_propagate_assign) {
    std::vector<Xor> xors;
    xors.emplace_back(true, lv{Lit(0), ~Lit(1)});

    SolverMock solver;

    solver.setTrail(lv{Lit(0)});

    EXPECT_CALL(solver, enqueue(Lit(1), PropEq(lv{Lit(1),~Lit(0)})));
    EXPECT_CALL(solver, conflict(_))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
    p.propagate();
}

TEST(XorPropagator, init_propagate_assign_2) {
    std::vector<Xor> xors;
    xors.emplace_back(false, lv{Lit(0), Lit(1), Lit(3)});
    xors.emplace_back(true, lv{Lit(3), Lit(4), Lit(5)});

    SolverMock solver;

    solver.setTrail(lv{Lit(0), ~Lit(1)});

    EXPECT_CALL(solver, enqueue(Lit(3), PropEq(lv{Lit(3), ~Lit(0), Lit(1)})));
    EXPECT_CALL(solver, conflict(_))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
    p.propagate();
}

TEST(XorPropagator, init_propagate_and_explicit_propagate) {
    std::vector<Xor> xors;
    xors.emplace_back(true, lv{Lit(0)});
    xors.emplace_back(true, lv{Lit(1)});

    SolverMock solver;
    EXPECT_CALL(solver, enqueue(Lit(0), PropEq(lv{Lit(0)})));
    EXPECT_CALL(solver, enqueue(Lit(1), PropEq(lv{Lit(1)})));
    EXPECT_CALL(solver, conflict(_))
        .Times(0);

    xorp::Solver s(solver);
    XorPropagator p(s, xors);
    p.propagate();
}