#pragma once

#include "prooflogging/ConstraintId.hpp"

namespace testImpl {
    typedef uint32_t LitData;

    class Var {
    public:
        LitData value;

        Var() {}

        Var(LitData value_)
            : value(value_)
        {}

        operator size_t() const {
            return value;
        }
    };


    class Lit {
    private:
        LitData value;



    public:
        Lit(){}

        Lit(int t)
            : value((std::abs(t) << 1) + (t < 0 ? 1 : 0))
        {}

        Lit(Var var, bool negated)
            : value((var.value << 1) + static_cast<LitData>(negated))
        {}

        Var var() const {
            return Var(value >> 1);
        }

        bool isNegated() const {
            return value & 1;
        }

        Lit operator~() const {
            Lit res;
            res.value = value ^ 1;
            return res;
        }

        bool operator==(const Lit& other) const {
            return value == other.value;
        }

        bool operator!=(const Lit& other) const {
            return value != other.value;
        }

        bool operator>(const Lit& other) const {
            return value > other.value;
        }

        operator size_t() const {
            return value;
        }

        explicit operator int64_t() const {
            int64_t res = this->var();
            if (this->isNegated()) {
                res *= -1;
            }
            return res;
        }


        static Lit Undef() {
            return Lit(0);
        };
    };

    class Clause {
    public:
        std::vector<Lit> lits;
        proof::ConstraintId id;

        Clause(){}

        Clause(std::initializer_list<Lit> _lits)
            : lits(_lits)
        {
            static size_t constraintCounter = 0;
            // convenience initiallization for test functions
            id = proof::ConstraintId{++constraintCounter};
        }

    };

    inline std::ostream& operator<<(std::ostream& os, const Var& v) {
        os << "x" << v.value;
        return os;
    }

    inline std::ostream& operator<<(std::ostream& os, const Lit& v) {
        if (v.isNegated()) {
            os << "~";
        }
        os << v.var();
        return os;
    }
}