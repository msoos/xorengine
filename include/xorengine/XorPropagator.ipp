#pragma once

#include "xorengine/XorPropagator.hpp"
#include "xorengine/private/myXor.hpp"
#include "xorengine/private/myXor.ipp"

namespace xorp {

template<typename Types>
XorPropagator<Types>::XorPropagator(
        xorp::Solver<Types>& solver,
        std::vector<Xor<Types>>& xors,
        proof::Proof<Types>* proof)
    : pimpl(std::move(std::make_unique<XorPropagatorImpl<Types>>(solver, xors, proof)))
{}

template<typename Types>
void XorPropagator<Types>::propagate() {
    pimpl->propagate();
}

template<typename Types>
void XorPropagator<Types>::notifyBacktrack(){
    pimpl->notifyBacktrack();
}

template<typename Types>
XorPropagator<Types>::~XorPropagator() = default;

} // end of namespace