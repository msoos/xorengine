#pragma once

#include "prooflogging/Proof.hpp"

namespace xorp {
    template<typename Types>
    struct Xor;
    class BDD;
}

namespace proof {
    namespace without {
        namespace xr {
            template<typename Types>
            class XorHandle {};

            template<typename Types>
            XorHandle<Types> newXorHandleFromProofTree(
                without::Proof<Types>&,
                xorp::Xor<Types>&,
                xorp::BDD&)
            {return XorHandle<Types>();}

            template<typename Types>
            XorHandle<Types> xorFromEquality(ConstraintId, ConstraintId)
            {return XorHandle<Types>();}

            template<typename Types>
            XorHandle<Types> xorSum(
                without::Proof<Types>&,
                const std::vector<XorHandle<Types>>&)
            {return XorHandle<Types>();}

            template<typename Types>
            ConstraintId reasonGeneration(
                without::Proof<Types>&,
                const XorHandle<Types>&,
                const std::vector<typename Types::Lit>&)
            {return ConstraintId{1};}

            template<typename Types>
            void deleteXor(
                without::Proof<Types>&,
                const XorHandle<Types>&)
            {}
        }
    }
}