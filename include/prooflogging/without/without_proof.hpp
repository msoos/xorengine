#pragma once

#include "xorengine/SolverAdapter.hpp"
#include "prooflogging/ConstraintId.hpp"

namespace proof {
namespace without {
    template<typename Types>
    class Proof {
    public:
        Proof(std::string, xorp::Solver<Types>&, size_t numConstraints = 0)
        {}

        ConstraintId getNextFormulaConstraintId() {
            return ConstraintId{1};
        }

        void comment(std::string comment) {
        }
    };
}
}