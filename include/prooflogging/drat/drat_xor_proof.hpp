#pragma once
#include "prooflogging/Proof.hpp"
#include "prooflogging/ConstraintId.hpp"
#include <memory>

namespace xorp {
    template<typename Types>
    struct Xor;
    class BDD;
}

namespace proof {
    namespace drat {
        namespace xr {
            template<typename Types>
            class LongXor;

            template<typename Types>
            class XorHandle {
            public:
                std::unique_ptr<LongXor<Types>> xr;
                XorHandle();
                XorHandle(XorHandle&& other);
                XorHandle(LongXor<Types>&& xr);
                XorHandle(const XorHandle& other);
                XorHandle& operator=(XorHandle&& other);
                XorHandle& operator=(const XorHandle& other);
                ~XorHandle();
            };

            template<typename Types>
            XorHandle<Types> newXorHandleFromProofTree(
                drat::Proof<Types>& proof,
                xorp::Xor<Types>& xr,
                xorp::BDD& proofTree);

            template<typename Types>
            XorHandle<Types> xorSum(
                drat::Proof<Types>& proof,
                const std::vector<XorHandle<Types>>& v);

            template<typename Types>
            ConstraintId reasonGeneration(
                drat::Proof<Types>& proof,
                const XorHandle<Types>& xr,
                const std::vector<typename Types::Lit>& reasonClause);

            template<typename Types>
            void deleteXor(
                drat::Proof<Types>& proof,
                const XorHandle<Types>& xr);
        }
    }
}